var docImages = function preloadImages() {
    var images = {
        'play': 'static/cassette/img/play.png',
        'pause': 'static/cassette/img/pause.png',
        'volume-on': 'static/cassette/img/volume-on.png',
        'volume-off': 'static/cassette/img/volume-off.png',
        'cassette-body': 'static/cassette/img/cassette-body.png',
        'cassette-body-s': 'static/cassette/img/cassette-body-s.png',
        'cassette-detail': 'static/cassette/img/cassette-detail.png',
        'bg-classic': 'static/cassette/img/bg-classic.jpg',
        'bg-jazz': 'static/cassette/img/bg-jazz.jpg',
        'bg-loff': 'static/cassette/img/bg-lofi.jpg',
        'album': 'static/cassette/img/album.jpg',
    };

    var preloaded = [];

    for (var img in images) {
        t = new Image();
        t.src = images[img];
        preloaded.push(t);
    }

    return images;
}();


// Radio Player
const Player = function (stations) {
    this.stations = stations;
    this.isPlaying = false;
    this.index = 1;
    this.vol = 0.5;
}


Player.prototype = {
    play: function (index) {
        var sound;
        var data = this.stations[index];

        if (data.howl) {
            sound = data.howl;
        } else {
            sound = data.howl = new Howl({
                src: data.url,
                html5: true,
                format: ['ogg', 'mp3', 'acc'],
            });
        }

        if (sound) {
            sound.volume(this.vol);
            sound.play();
            this.isPlaying = true;
            uiToggleOnPlaying(1);
        }
    },

    stop: function (index) {
        var sound = this.stations[index].howl;

        if (sound) {
            sound.once('stop', () => { sound.fade(sound.volume(), 0, 1000); });

            sound.unload();
            this.isPlaying = false;
            uiToggleOnPlaying(0);

        }

    },

    update: function (index) {
        prev = this.isPlaying;
        this.stop(this.index);
        this.index = index;
        if (prev)
            this.play(this.index);
    },

    volume: function (val) {
        var sound = this.stations[this.index].howl;
        if (sound) {
            this.vol = val;
            sound.volume(this.vol);
        }
    },
};


$(window).on('load', function () {

    var stations = getStations();

    if (stations) {

        var player = new Player(stations);

        $('#play-btn').on('click', function () {
            if (!player.isPlaying) {
                player.play(player.index);

            } else {
                player.stop(player.index)
            }
        });

        $('#lofi-btn, #classic-btn, #jazz-btn').on('click', function () {
            if (getStationIndex(this.name, stations) != player.index) {
                i = getStationIndex(this.name, stations)
                player.update(i);
                uiSwitchStation(this.id, stations);
                uiSwitchBackground(getStationGenre(i, stations));
            }
        });

        $('#volume_bar').on('mousedown', function () {
            $('#volume_bar').on('input', function () {
                player.volume(this.value / this.max);
            });
        });


        $('#volume_bar').on('touchstart', function () {
            $('#volume_bar').on('input', function () {
                player.volume(this.value / this.max);
            });
        });

        $('#volume_bar').on('click', function () {
            player.volume(this.value / this.max);
        });


        $('#volume_mute').on('click', function () {
            Howler.mute((Howler._muted) ? 0 : 1);
        });

    } else {
        $('#play-btn').on('click', function () {
            loadStationError();
        });

        loadStationError();
    }


});


function uiToggleOnPlaying(state) {
    if (state) {
        $('#play-btn').removeClass('play');
        $('#play-btn').addClass('pause');
        $('#play-btn').addClass('in_active');

        $('#roller_right, #roller_left').addClass('rotate');
    } else {
        $('#play-btn').removeClass('pause');
        $('#play-btn').removeClass('in_active');
        $('#play-btn').addClass('play');
        $('#roller_right, #roller_left').removeClass('rotate');
    }
}

function uiSwitchStation(selected, stations, genre) {
    // change layout
    $('#lofi-btn, #classic-btn, #jazz-btn').removeClass('in_active');
    $('#' + selected).addClass('in_active');
}

function uiSwitchBackground(genre) {
    $('body').removeClass($('body').attr('class'));
    $('body').addClass(genre);
    $('body').addClass('bg_change');
    setTimeout(function () { $('body').removeClass('bg_change'); }, 1600)
}

function loadStationError() {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "500",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr["error"]("Не удалось получить список радиостанций.", "Ошибка")


}


function getStationGenre(id, stations) {
    return stations[id].genre.toLowerCase();
}

function getStationIndex(genre, stations) {
    return stations.map(function (o) {
        return o.genre.toLowerCase()
    }).indexOf(genre);
}


function getStations() {
    var data;
    const csrftoken = $.cookie('csrftoken');
    $.ajax({
        headers: {
            'X-CSRFToken': csrftoken
        },
        type: 'POST',
        url: "/api/getStations",
        dataType: 'json',
        async: false,
        success: function (response) {
            data = response;
        },
        error: function (error) {
            toastr.error(error);
        }
    });
    return data;
}

particlesJS("particles", {
    "particles": {
        "number": {
            "value": 40,
            "density": {
                "enable": true,
                "value_area": 800
            }
        },
        "color": {
            "value": "#FAEE93"
        },
        "shape": {
            "type": "circle",
            "stroke": {
                "width": 0,
                "color": "#000000"
            },
            "polygon": {
                "nb_sides": 5
            },
            "image": {
                "src": "img/github.svg",
                "width": 50,
                "height": 50
            }
        },
        "opacity": {
            "value": 1,
            "random": true,
            "anim": {
                "enable": true,
                "speed": 1,
                "opacity_min": 0,
                "sync": false
            }
        },
        "size": {
            "value": 3,
            "random": true,
            "anim": {
                "enable": false,
                "speed": 4,
                "size_min": 0.3,
                "sync": false
            }
        },
        "line_linked": {
            "enable": false,
            "distance": 150,
            "color": "#ffffff",
            "opacity": 0.4,
            "width": 1
        },
        "move": {
            "enable": true,
            "speed": 1,
            "direction": "none",
            "random": true,
            "straight": false,
            "out_mode": "out",
            "bounce": true,
            "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 600
            }
        }
    },
    "interactivity": {
        "detect_on": "canvas",
        "events": {
            "onhover": {
                "enable": false,
                "mode": "bubble"
            },
            "onclick": {
                "enable": false,
                "mode": "repulse"
            },
            "resize": true
        },
        "modes": {
            "grab": {
                "distance": 400,
                "line_linked": {
                    "opacity": 1
                }
            },
            "bubble": {
                "distance": 250,
                "size": 0,
                "duration": 2,
                "opacity": 0,
                "speed": 3
            },
            "repulse": {
                "distance": 400,
                "duration": 0.3
            },
            "push": {
                "particles_nb": 4
            },
            "remove": {
                "particles_nb": 2
            }
        }
    },
    "retina_detect": true
});
