from django.contrib import admin
from .models import Station, Playlist, SongFile


class FileAdmin(admin.StackedInline):
    model = SongFile


@admin.register(Playlist)
class PlaylistAdmin(admin.ModelAdmin):
    inlines = [FileAdmin]


@admin.register(Station)
class StationAdmin(admin.ModelAdmin):
    pass
