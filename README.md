# Cassette Project

### Тут будет описание...
<br>

## В разработке применяется:

* [Python 3.8.5+](https://www.python.org/downloads/release/python-385/)
* [Django 3](https://www.djangoproject.com/)
* [Icecast](https://icecast.org/)
* [Liquidsoap](https://www.liquidsoap.info/)
* [howler.js](https://howlerjs.com/)

## Установка
### Создание виртуального окружения
```
python -m venv venv
```
### Активация виртуального окружения
###### [Windows]
```
venv\Scripts\activate.bat
```
###### [Linux]
```
source venv/bin/activate
```
### Установка пакетов
```
pip install -r requirements.txt
```
### Подготовка базы данных
```
python manage.py makemigrations
```
```
python manage.py migrate
```
### Запуск сервера
```
python manage.py runserver
```
## Запуск в Docker (локально)
### Запуск docker-compose
```
docker-compose -f docker-compose-dev.yml up --build
```
